/* GPIO numbers for each signal. Negative values are invalid */
#define tck_gpio 27
#define tms_gpio 24
#define tdi_gpio 23
#define tdo_gpio 22
#define led      18

/* Statuses */
#define RUNNING 1
#define STOP    2
#define STOPPED 0

/* Exposed functions: */
int start(int port);
void stop();