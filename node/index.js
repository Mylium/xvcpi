const addon = require('bindings')('xvcpi'); // import 'xvcpi.node'
exports.start = function() {
    return new Promise((resolve, reject) => {
        // Randomise port:
        const port = 2500 + Math.floor(Math.random()*1000) // between 2500 and 3500
        try {
            addon.start(port, () => console.log)
            resolve()
        } catch(err) {
            reject(err)
        }
    })
}
exports.stop = addon.stop
exports.info = addon.info