const xvcpi = require('./build/Release/xvcpi.node')

console.log(xvcpi)

console.log("Starting test server on port 3000...")
xvcpi.start(3000, (err, result) => {
    console.log("Test server stopped.")
    if(err)
        console.log(err);
    console.log(`result: ${result}`)
})

setTimeout(() => {
    console.log("Stopping...")
    xvcpi.stop()
}, 10000)
